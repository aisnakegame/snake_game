from uuid import uuid4

import bson
from pygame import Vector2

from mongo_handler import MongoHandler


class LoadedGameHandler:
    def __init__(self, game_settings):
        self.mongo_handler = MongoHandler(game_settings.mongo_url, game_settings.mongo_db, game_settings.mongo_collection)
        self.game_document = self.load_document(game_settings.loaded_game_id) if game_settings.loaded_game_id is not None else None
        self.fruit_game_index = 0

    def load_document(self, game_id):
        return self.mongo_handler.get_game(game_id)

    def get_fruit_location(self):
        fruits_dict = (self.game_document.get("GameFruits"))
        fruit_position = Vector2(fruits_dict[self.fruit_game_index].get('x'), fruits_dict[self.fruit_game_index].get('y'))
        self.fruit_game_index += 1
        return  fruit_position

