import pygame
from pygame.math import Vector2

from mongo_handler import MongoHandler


class Snake:
    def __init__(self, game_settings, fruit):
        self.snake_body = [Vector2(5, 10)]
        self.snake_direction = Vector2(0, 0)
        self.game_settings = game_settings
        self.fruit = fruit
        self.score = 0
        self.mongo_handler = MongoHandler(game_settings.mongo_url, game_settings.mongo_db,
                                          game_settings.mongo_collection)

    def draw_snake(self):
        for block in self.snake_body:
            x_pos = int(block.x * self.game_settings.cell_size)
            y_pos = int(block.y * self.game_settings.cell_size)
            block_rect = pygame.Rect(x_pos, y_pos, self.game_settings.cell_size, self.game_settings.cell_size)
            pygame.draw.rect(self.game_settings.screen, (94, 177, 191), block_rect)

    def change_position(self):
        if len(self.snake_body) == 1:
            self.snake_body[0] += self.snake_direction
        else:
            body_copy = self.snake_body[:-1]
            body_copy.insert(0, body_copy[0] + self.snake_direction)
            self.snake_body = body_copy

    def add_piece(self):
        latest_vector = Vector2(self.snake_body[len(self.snake_body) - 1].x - self.snake_direction.x,
                                self.snake_body[len(self.snake_body) - 1].y - self.snake_direction.y)

        self.snake_body.append(latest_vector)
        self.score += 1

    def check_collusion(self):
        if self.snake_body[0].x == self.fruit.position.x and self.snake_body[0].y == self.fruit.position.y:
            self.add_piece()
            self.fruit.set_position()
            self.mongo_handler.add_fruit_location(self.fruit.position)

    def check_piece_collusion(self):
        if self.snake_body[0].x == self.fruit.position.x and self.snake_body[0].y == self.fruit.position.y:
            return 1000

    def handle_direction_change(self, new_direction):
        if self.snake_direction.x + new_direction.x != 0 or self.snake_direction.y + new_direction.y != 0:
            self.snake_direction = new_direction

    def check_loss(self, cells_number):
        if self.check_out_of_area(cells_number):
            return True
        for block_index in range(1, len(self.snake_body)):
            if self.snake_body[0].x == self.snake_body[block_index].x and self.snake_body[0].y == self.snake_body[
                block_index].y:
                return True
        return False

    def check_out_of_area(self, cells_number):
        return self.snake_body[0].x >= cells_number - 1 or self.snake_body[0].x <= 0 or \
               self.snake_body[0].y >= cells_number - 1 or self.snake_body[0].y <= 0
