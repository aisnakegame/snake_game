from pygame import Vector2


class AiSnakeHandling:
    def __init__(self, snake, turns_ahead=7):
        self.snake = snake
        self.ai_turns_ahead = turns_ahead

    def check_possible_loss(self, direction, cells_number):
        if self.check_possible_out_of_range(cells_number, direction):
            return True
        for block_index in range(1, len(self.snake.snake_body)):
            if self.snake.snake_body[0].x + direction.x == self.snake.snake_body[block_index].x and self.snake.snake_body[
                0].y + direction.y == self.snake.snake_body[
                block_index].y:
                return True
        return False

    def check_out_of_area(self, cells_number):
        return self.snake.snake_body[0].x >= cells_number - 1 or self.snake.snake_body[0].x <= 0 or \
               self.snake.snake_body[0].y >= cells_number - 1 or self.snake.snake_body[0].y <= 0

    def check_possible_out_of_range(self, cells_number, direction):
        return self.snake.snake_body[0].x + direction.x >= cells_number - 1 or self.snake.snake_body[0].x + direction.x <= 0 or \
               self.snake.snake_body[0].y + direction.y >= cells_number - 1 or self.snake.snake_body[0].y + direction.y <= 0

    def ai_play(self):
        best_direction = self.best_move(self.snake)
        self.snake.snake_direction = best_direction

    def check_status(self, cells_number):
        if self.snake.check_loss(cells_number):
            return -1000
        if self.snake.check_piece_collusion():
            return 1000
        return 0

    def eval_function(self):
        eval = (pow((self.snake.snake_body[0].x - self.snake.fruit.position.x), 2) + pow(
            (self.snake.snake_body[0].y - self.snake.fruit.position.y), 2)) * -1
        if eval == 0:
            return 1000
        return eval

    def mini_max(self, snake, depth, max_depth):
        status = self.check_status(snake.game_settings.cell_number)
        if status != 0:
            return status

        if depth >= max_depth:
            return self.eval_function()

        best_score = -1000
        for possible_direction in self.possible_directions():
            data_temp = self.clone_positions()
            snake.snake_direction = possible_direction
            snake.change_position()
            score = self.mini_max(snake, depth + 1, max_depth)
            if score > best_score:
                best_score = score
            snake.snake_body = data_temp[0]
            snake.fruit.position = data_temp[1]
            snake.snake_direction = data_temp[2]
        return best_score

    def best_move(self, snake):
        best_score = -10000
        best_move = Vector2(0, 0)
        for possible_move in self.possible_directions():
            data_temp = self.clone_positions()
            snake.snake_direction = possible_move
            snake.change_position()
            score = self.mini_max(snake, 0, self.ai_turns_ahead)
            if score > best_score:
                best_score = score
                best_move = possible_move
            snake.snake_body = data_temp[0]
            snake.snake_direction = data_temp[2]
            snake.fruit.position = data_temp[1]
        return best_move

    def clone_positions(self):
        snake_body = []
        for block in self.snake.snake_body:
            snake_body.append(Vector2(block.x, block.y))
        fruit_position = Vector2(self.snake.fruit.position.x, self.snake.fruit.position.y)
        direction = Vector2(self.snake.snake_direction.x, self.snake.snake_direction.y)

        return [snake_body, fruit_position, direction]

    def possible_directions(self):
        all_possible = [Vector2(1, 0), Vector2(-1, 0), Vector2(0, 1), Vector2(0, -1)]
        for option in all_possible:
            if self.snake.snake_direction.x + option.x != 0 or self.snake.snake_direction.y + option.y != 0:
                yield option
