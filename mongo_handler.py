import bson
import pymongo
from bson.binary import UuidRepresentation
from uuid import uuid4, UUID


class MongoHandler:
    def __init__(self, mongo_url, db_name, collection_name):
        self.mongo_client = pymongo.MongoClient(f"mongodb://{mongo_url}/")
        self.db_name = db_name
        self.collection_name = collection_name
        self.fruit_locations = []

    def add_fruit_location(self, fruit_location):
        self.fruit_locations.append({"x": fruit_location.x, "y": fruit_location.y})

    def save_game(self):
        uuid_obj = uuid4()
        game_id = bson.Binary.from_uuid(uuid_obj)
        mongo_db = self.mongo_client[self.db_name]
        collection = mongo_db[self.collection_name]

        game_item = {"GameId": str(game_id), "GameFruits": self.fruit_locations}
        collection.insert_one(game_item)

    def get_game(self, game_id):
        mongo_db = self.mongo_client[self.db_name]
        collection = mongo_db[self.collection_name]
        return collection.find_one({"GameId": game_id})

