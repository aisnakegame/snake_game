import random

import pygame
from pygame import Vector2

from load_game_handler import LoadedGameHandler


class Fruit:
    def __init__(self, game_settings):
        self.game_settings = game_settings
        self.position = Vector2()
        self.loaded_game_handler = LoadedGameHandler(game_settings)
        self.set_position()

    def draw_fruit(self):
        fruit_rect = pygame.Rect(int(self.position.x * self.game_settings.cell_size),
                                 int(self.position.y * self.game_settings.cell_size),
                                 self.game_settings.cell_size,
                                 self.game_settings.cell_size)
        pygame.draw.rect(self.game_settings.screen, (238, 96, 85), fruit_rect)

    def set_position(self):
        if self.loaded_game_handler.game_document is not None:
            self.position = self.loaded_game_handler.get_fruit_location()
        else:
            x_position = random.randint(2, self.game_settings.cell_number - 2)
            y_position = random.randint(2, self.game_settings.cell_number - 2)
            self.position = Vector2(x_position, y_position)
