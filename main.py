import sys

import pygame
from pygame import Vector2
from ai_handling import AiSnakeHandling
from fruit import Fruit
from game_settings import GameSettings
from snake import Snake


def init_game():
    pygame.init()
    cell_size = 40
    cell_number = 20
    screen_update_time = 50
    screen_main_color = (175, 215, 70)
    ai_turns_ahead = 5
    on_pause = False
    # when off, increase the `screen_update_time` property
    is_ai_play = True
    mongo_url = "localhost:27017"
    mongo_db_name = "SnakeGame"
    mongo_collection_name = "AiGames"

    screen = pygame.display.set_mode((cell_number * cell_size, cell_number * cell_size))
    game_settings = GameSettings(cell_size, cell_number, screen, mongo_url, mongo_db_name, mongo_collection_name)
    fruit = Fruit(game_settings)
    snake = Snake(game_settings, fruit)

    screen_update = pygame.USEREVENT
    pygame.time.set_timer(screen_update, screen_update_time)

    screen.fill(screen_main_color)

    ai_handling = AiSnakeHandling(snake, ai_turns_ahead)

    if is_ai_play:
        snake.snake_direction = Vector2(1, 0)

    while True:
        screen.fill((175, 215, 70))
        paint_screen(cell_number, cell_size, screen)
        text_to_screen(screen, f"score: {snake.score}", 0, 0 / 2, 40)

        for event in pygame.event.get():

            if not on_pause and not is_ai_play and event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    snake.handle_direction_change(Vector2(0, -1))
                if event.key == pygame.K_DOWN:
                    snake.handle_direction_change(Vector2(0, 1))
                if event.key == pygame.K_RIGHT:
                    snake.handle_direction_change(Vector2(1, 0))
                if event.key == pygame.K_LEFT:
                    snake.handle_direction_change(Vector2(-1, 0))

            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if not on_pause and event.type == screen_update:
                if is_ai_play:
                    ai_handling.ai_play()
                snake.change_position()
                snake.check_collusion()
                if snake.check_loss(cell_number):
                    text_to_screen(screen, "Game Over", (cell_number * cell_size) / 3,
                                   (cell_number * cell_size) / 2)
                    update_screen(snake, fruit)
                    on_pause = True
                    snake.mongo_handler.save_game()

            if not on_pause:
                update_screen(snake, fruit)


def text_to_screen(screen, text, x, y, size=50, color1=(0, 255, 0), color2=(0, 0, 0)):
    text = str(text)
    font = pygame.font.Font('freesansbold.ttf', size)
    text = font.render(text, True, color1, color2)
    screen.blit(text, (x, y))


def update_screen(snake, fruit):
    fruit.draw_fruit()
    snake.draw_snake()
    pygame.display.update()


def paint_screen(cell_number, cell_size, screen):
    for block_index_x in range(0, cell_number):
        for block_index_y in range(0, cell_number):
            if block_index_x == 0 or block_index_x == cell_number - 1 \
                    or block_index_y == 0 or block_index_y == cell_number - 1:
                block_rect = pygame.Rect(block_index_x * cell_size, block_index_y * cell_size, cell_size, cell_size)
                pygame.draw.rect(screen, (92, 131, 78), block_rect)


if __name__ == '__main__':
    init_game()
