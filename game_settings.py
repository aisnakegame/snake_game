
class GameSettings:
    def __init__(self, cell_size, cell_number, screen, mongo_url, mongo_db, mongo_collection, loaded_game_id=None):
        self.cell_size = cell_size
        self.cell_number = cell_number
        self.screen = screen
        self.mongo_url = mongo_url
        self.mongo_db = mongo_db
        self.mongo_collection = mongo_collection
        self.loaded_game_id = loaded_game_id
